﻿using game.api;
using game.api.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace game.infrastructure
{
    public class XmlGameSaver : ISaveMethods
    {
        public XmlGameSaver(object objectToXML, string path)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<CheckPoint>));
            using var stream = new StreamWriter(path);
            //using var mStream = new MemoryStream
            try
            {
                serializer.Serialize(stream, objectToXML);
            }
            finally
            {
                stream.Close();
            }
        }
    }
}
