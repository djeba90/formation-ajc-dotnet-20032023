﻿using game.api;
using game.api.Adapters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace game.infrastructure
{
    public class JsonGameSaver : IGameSauvegarde
    {
        private readonly string chemin;

        public JsonGameSaver(string chemin)
        {
            this.chemin = chemin;
        }

        public void SaveAll(List<CheckPoint> checkPoints)
        {
            this.Save(checkPoints);
        }

        public void SaveOne(CheckPoint checkPoint)
        {
            this.Save(checkPoint);
        }

        public void Save(object objectToJson)
        {
            var json = JsonConvert.SerializeObject(objectToJson, Newtonsoft.Json.Formatting.Indented);
            try
            {
                File.WriteAllText(this.chemin, json);
            }
            catch (IOException ex)
            {

                Console.WriteLine("DAS IST CASSÉ " + ex);
            }
        }
    }
}
