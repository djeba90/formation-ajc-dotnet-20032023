﻿using game.api;
using game.api.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.infrastructure
{
    public class PureGameSaver : ISaveMethods
    {
        public PureGameSaver(object objectToPure, string path) 
        {
            String concatDegueu = "";
            foreach (var checkPoint in (List<CheckPoint>)objectToPure)
            {
                concatDegueu += checkPoint.ToString();
            }

            try
            {
                File.WriteAllText(path, concatDegueu);
            }
            catch (IOException ex)
            {
                Console.WriteLine("DAS IST CASSÉ " + ex);
            }
        }
    }
}
