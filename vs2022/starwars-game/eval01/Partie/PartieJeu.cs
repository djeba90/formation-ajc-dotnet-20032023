﻿namespace Partie
{
    public class PartieJeu
    {
        private DateTime date = DateTime.Now;
        private List<Checkpoint> checkpoints = new List<Checkpoint>();

        public PartieJeu(DateTime date, List<Checkpoint> checkpoints)
        {
            this.Date = date;
            this.Checkpoints = checkpoints;
        }

        public DateTime Date { get => date; set => date = value; }
        public List<Checkpoint> Checkpoints { get => checkpoints; set => checkpoints = value; }
    }
}