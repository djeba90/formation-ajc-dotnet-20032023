﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Partie
{
    public class Attaque
    {
        private string name;
        private int damage;
        private int portee;

        public Attaque(string name, int damage, int portee)
        {
            this.Name = name;
            this.Damage = damage;
            this.Portee = portee;
        }

        public string Name { get => name; set => name = value; }
        public int Damage { get => damage; set => damage = value; }
        public int Portee { get => portee; set => portee = value; }

        public override string ToString()
        {
            return $"Tu as lancé {Name}, c'est très efficace";
        }
    }
}
