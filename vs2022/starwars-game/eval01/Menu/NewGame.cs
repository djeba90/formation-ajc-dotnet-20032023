﻿using Partie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Menu
{
    /// <summary>
    /// Classe de nouvelle partie qui permet de démarrer et d'afficher la grille
    /// </summary>
    public class NewGame : MenuJeu
    {
        Afficher afficherAvecSaut = Console.WriteLine;
        Afficher afficherLigne = Console.Write;
        public NewGame(string name) : base(name)
        {
        }



        public override void LancerPartie(PartieJeu partie1, List<Ennemie> ennemi, List<Player> player)
        {
            Dictionary<int,int> coordonneMechant = new Dictionary<int,int>();
            Dictionary<int,int> coordonneGentil = new Dictionary<int,int>();

            foreach (var item in ennemi)
            {
                try
                {
                    coordonneMechant.Add(item.CoordonneX, item.CoordonneY);
                }catch (Exception ex)
                {
                    afficherAvecSaut("Pas de place pour " + item.Name);
                }
            }
            foreach(var item in player)
            {
                try
                {
                    coordonneGentil.Add(item.CoordonneX, item.CoordonneY);
                }
                catch (Exception ex)
                {
                    afficherAvecSaut("Pas de place pour " + item.Name);
                }
            }
            afficherCarte(coordonneGentil, coordonneMechant);
        }

        public void afficherCarte(Dictionary<int, int> positionGentil, Dictionary<int, int> positionMechant)
        {
            int[,] grille = new int[10,10];

            foreach (var item in positionGentil)
            {
                grille[item.Key,item.Value] = 1;
            }
            foreach (var item in positionMechant)
            {
                grille[item.Key, item.Value] = 2;
            }
            for(int i = 0; i < 10; i++)
            {
                for(int j = 0; j < 10; j++)
                {
                    if (grille[i,j] == 1)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                    }else if (grille[i,j] == 2)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                    }
                    afficherLigne(grille[i,j]);

                }
                afficherAvecSaut("");
            }
        }
    }
}
