﻿using Partie;

namespace Menu
{
    /// <summary>
    /// Définition des différents menus dispo
    /// </summary>
    public abstract class MenuJeu
    {
        private string name;

        protected MenuJeu(string name)
        {
            this.Name = name;
        }

        public string Name { get => name; set => name = value; }

        /// <summary>
        /// besoin minimum pour charger ou créer une partie
        /// </summary>
        /// <param name="partie1"></param>
        /// <param name="ennemi"></param>
        /// <param name="player"></param>
        public virtual void LancerPartie(PartieJeu partie1, List<Ennemie> ennemi, List<Player> player)
        {

        }
        public override string ToString()
        {
            return $"{name}";
        }

        
    }
}