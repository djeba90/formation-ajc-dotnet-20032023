﻿using Partie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu
{
    /// <summary>
    /// chargement de la partie
    /// </summary>
    public class LoadGame : MenuJeu
    {
        public LoadGame(string name) : base(name)
        {
        }
        public override void LancerPartie(PartieJeu partie1, List<Ennemie> ennemi, List<Player> player)
        {
            base.LancerPartie(partie1, ennemi, player);
        }

        
    }
}
