﻿// See https://aka.ms/new-console-template for more information
using Menu;
using Partie;
using starwars_game;
using System.Numerics;


//Console.WriteLine($"{monTitre} {DateTime.Now.ToString("dd/MM/yyyy")} ");
//List<String> noms = new List<String>();

List<Attaque> attaquesPersonnage = new List<Attaque>();
List<Attaque> attaquesEnnemie = new List<Attaque>();
Attaque attack = new Attaque("LBD", 100, 100);
Attaque esquive = new Attaque("Bouclier antiémeute", 10, 0);
Attaque grrr = new Attaque("On a faim", 1, 1);
Attaque fuite = new Attaque("AHHHHH", 1, 1);

attaquesPersonnage.Add(fuite);
attaquesPersonnage.Add(grrr);
attaquesEnnemie.Add(attack);
attaquesEnnemie.Add(esquive);

List<Player> playerList = new List<Player>();
Player personnage1 = new Player("Francois", 100, attaquesPersonnage, 1,3);
Player personnage2 = new Player("Francois bis", 100, attaquesPersonnage, 2, 4);

playerList.Add(personnage1);
playerList.Add(personnage2);

List<Ennemie> ennemiesList = new List<Ennemie>();
Ennemie ennemie1 = new Ennemie("Arthur", 100, attaquesEnnemie, 6, 3);
Ennemie ennemie2 = new Ennemie("Arthur bis", 100, attaquesEnnemie, 7, 4);

Random randomEnemie = new Random();
Ennemie ennemieRandom = new Ennemie("Arthur bis bis", 100, attaquesEnnemie, randomEnemie.Next(1,10), randomEnemie.Next(1, 10));

ennemiesList.Add(ennemie1);
ennemiesList.Add(ennemie2);
ennemiesList.Add(ennemieRandom);

Dictionary<int, MenuJeu> monMenu = new Dictionary<int, MenuJeu>
{
    { 1, new NewGame("1 - Nouvelle partie") },
    { 2, new LoadGame("2 - Charger partie") },
    { 3, new Leave("3 - QUITTER") }
};

foreach (var menu in monMenu)
{
    Console.WriteLine(menu.Value);
}

//string choix = "";

var choix = getPrompt("Que choisis-tu ?");

List<Checkpoint> checkpoint = new List<Checkpoint>();
PartieJeu partie1 = new PartieJeu(DateTime.Now, checkpoint);
NewGame game1 = new NewGame("masave");

monMenu[int.Parse(choix)].LancerPartie(partie1, ennemiesList, playerList);




String getPrompt(String prompt)
{
    String repUtilisateur = "";
    Console.WriteLine(prompt);
    try
    {
        repUtilisateur = Console.ReadLine();
    }
    catch (Exception ex)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("MERCI DE RENTRER UNE VALEUR CORRECT");
        Console.ForegroundColor = ConsoleColor.White;
    }
    while (repUtilisateur == "")
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("MERCI DE RENTRER QUELQUE CHOSE");
        Console.ForegroundColor = ConsoleColor.White;
        Console.WriteLine(prompt);
        repUtilisateur = Console.ReadLine();
    }


        return repUtilisateur;
}


#region ancien code qui va peut être resservir 
//preparerEnnemis();

//String reponse = "";
/*do
{
    affichelist(noms);
    reponse = getPrompt("Donne le nom d'un ennemie");
    addEnnemis(reponse);

} while (reponse != "STOP");*/

//afficheTitre();


//afficheMenu();


/*int menuItem = 0;
do
{
    Console.WriteLine("");
    Console.ForegroundColor = ConsoleColor.White;
    Console.WriteLine("Que choisis-tu ?");
    menuItem = int.Parse(Console.ReadLine());

    switch ((MenuItemType)menuItem)
    {
        case MenuItemType.Demarrer_Partie:
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            testResult(getPrompt("Quel est ton prénom ?"), getPrompt("Quelle est ta date de naissance ?"), getPrompt("Tu as quel âge ?"));
            Console.ForegroundColor = ConsoleColor.Black;
            break;
        case MenuItemType.Charger_Partie:
            Console.WriteLine("SALUT");
            Console.ForegroundColor = ConsoleColor.Black;
            break;
        case MenuItemType.Quitter:
            Console.WriteLine("SALUT");
            Console.ForegroundColor = ConsoleColor.Black;
            break;
        default:
            Console.WriteLine("françois connard");
            break;
    }
} while (menuItem != 0);*/




/*Console.ForegroundColor = ConsoleColor.Yellow;
Console.WriteLine("A copycat of zelda".ToLower());*/

/*void afficheTitre()
{
    Random rand = new Random();
    String monTitre = "A STARWARS GAME !";
    string format = "{0} ( {1} ) - {1}";

    foreach (char c in string.Format(format, monTitre, DateTime.Now))
    {

        Console.ForegroundColor = (ConsoleColor)rand.Next(1, 16);
        Console.Write(c);

    }
    Console.WriteLine("");
}
void afficheMenu()
{
    Console.ForegroundColor = ConsoleColor.Blue;

    var listMenu = Enum.GetValues(typeof(MenuItemType));

    foreach (MenuItemType item in listMenu)
    {
        Console.WriteLine("_" + (int)item + " " + item.ToString().Replace("_", " "));
    }
}

void affiche(string prenom, TimeSpan age)
{
    Console.WriteLine($"Ton prénom est bien {prenom} ? Et tu es âgé de {(int)(age.TotalDays)} années ?");
}
void testResult(string prenom, string dateNaissance, string age)
{
    if (DateTime.TryParse(dateNaissance, out DateTime date))
    {
        TimeSpan ageTS = (DateTime.Now - date) / 365;
        try
        {
            int ageInt = Int16.Parse(age);
            if ((int)ageTS.TotalDays == ageInt && ageInt > 13)
            {
                affiche(prenom, ageTS);
            }
            else
            {
                int newAge;
                bool isValidAge = false;
                do
                {
                    string input = getPrompt("Tu as quel âge ?");
                    isValidAge = Int32.TryParse(input, out newAge) && (int)ageTS.TotalDays == newAge && newAge > 13;
                    if (!isValidAge)
                    {
                        Console.WriteLine("ÂGE INVALIDE !");
                    }
                } while (!isValidAge);

                affiche(prenom, ageTS);
            }
        }
        catch (OverflowException)
        {
            Console.WriteLine("T UN MECHANT (overflow exception)");
        }
    }
    else
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("MERCI DE RENTRER UNE DATE VALIDE");
        Console.ResetColor();
    }
}




void preparerEnnemis()
{
    noms.Add("Dark vador");
    noms.Add("Boba feet");
    noms.Add("Empereur palpatine");
    noms.Add("droide");

    List<int> powers = new List<int>() { 1, 2, 3, 4 };
    var sum = powers.Sum();
    var first = powers.Min();
    //Console.WriteLine(first);
    String[] nomAs =
    {
        "Dark vador",
        "Boba feet",
        "Empereur palpatine"
    };
    var min = nomAs.Min();
    //Console.WriteLine(min);


}
void affichelist(List<String> affiche)
{
    Console.ForegroundColor = ConsoleColor.Blue;
    foreach (String s in affiche)
    {
        Console.WriteLine(s);
    }
    Console.ForegroundColor = ConsoleColor.White;
    reqEnnemi();

}
void addEnnemis(string ennemiName)
{
    if (!noms.Contains(ennemiName))
    {
        noms.Add(ennemiName);
    }
    else
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("DEJA ADD");
        Console.ForegroundColor = ConsoleColor.White;

    }
}

void reqEnnemi()
{
    var query = from enem in noms let eneMaj =  enem[0].ToString().ToUpper() + enem.Substring(1).ToLower()
                select new {Maj = eneMaj };

    *//*var query2 = noms.Where(item => item.StartsWith("D") || item.EndsWith("D"))
                 .OrderByDescending(item => item)
                 .Select(item => item);*//*

    foreach (var item in query)
    {
        Console.WriteLine(item);
    }
}*/
#endregion