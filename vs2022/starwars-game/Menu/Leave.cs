﻿using Partie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu
{
    /// <summary>
    /// pour quitter la partie
    /// </summary>
    public class Leave : MenuJeu
    {
        public Leave(string name) : base(name)
        {
        }

        public override void LancerPartie(PartieJeu partie1, List<Ennemie> ennemi, List<Player> player)
        {
            
        }
    }
}
