﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Partie
{
    public class Checkpoint
    {

        private DateTime date = DateTime.Now;
        private int PV;
        private int CoordonneeX;
        private int CoordonneeY;

        public Checkpoint(int pv, int coordonneeX, int coordonneeY)
        {
            PV1 = pv;
            CoordonneeX1 = coordonneeX;
            CoordonneeY1 = coordonneeY;
        }

        public DateTime Date { get => date; set => date = value; }
        public int PV1 { get => PV; set => PV = value; }
        public int CoordonneeX1 { get => CoordonneeX; set => CoordonneeX = value; }
        public int CoordonneeY1 { get => CoordonneeY; set => CoordonneeY = value; }


    }
}
