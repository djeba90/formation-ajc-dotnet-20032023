﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Partie
{
    /// <summary>
    /// Classe parent des joueurs et ennemis
    /// </summary>
    public class Personnage
    {
        private string name;
        private int PV;
        private List<Attaque> attaques = new List<Attaque>();
        private int coordonneX;
        private int coordonneY;
        public Personnage(string name, int pV, List<Attaque> attaques, int coordX, int coordY)
        {
            this.name = name;
            PV = pV;
            this.Attaques = attaques;
            this.coordonneX = coordX;
            this.coordonneY = coordY;
        }

        public string Name { get => name; set => name = value; }
        public int PV1 { get => PV; set => PV = value; }
        public List<Attaque> Attaques { get => attaques; set => attaques = value; }
        public int CoordonneX { get => coordonneX; set => coordonneX = value; }
        public int CoordonneY { get => coordonneY; set => coordonneY = value; }

        public void Attaquer()
        {
            Attaques.ToString();
        }

        public void Defense()
        {
            Attaques.ToString();
        }
    }
}
